import { Component, OnInit } from '@angular/core';
import { Post } from '../_model/post.model';
import { PostService } from '../_services/post.service';

@Component({
  selector: 'app-user-feed',
  templateUrl: './user-feed.component.html',
  styleUrls: ['./user-feed.component.css']
})
export class UserFeedComponent implements OnInit {

  posts: Post[]

  constructor(private postService: PostService) { }


  ngOnInit(): void {
    this.getUsers();
  }

  private getUsers(){
    this.postService.getPostsList().subscribe(data => {
      this.posts = data;
    });
  }
}
