package com.socialmedia.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.socialmedia.entity.User;

@Repository
public interface UserDao extends CrudRepository<User, String> {
}
