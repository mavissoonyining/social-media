package com.socialmedia.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.socialmedia.dao.PostDao;
import com.socialmedia.entity.Post;

@Service
public class PostService {

    @Autowired
    private PostDao postDao;

    public Post createNewPost(Post post) {
        return postDao.save(post);
    }
}
