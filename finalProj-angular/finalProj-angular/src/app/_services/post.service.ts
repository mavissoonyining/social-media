import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../_model/post.model';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  PATH_OF_API = 'http://localhost:8080/api/post';

  constructor(private httpClient: HttpClient) { }

  public addPost (post: FormData){
    return this.httpClient.post<Post>(this.PATH_OF_API, post);
  }

  public getPostsList(): Observable<Post[]>{
    return this.httpClient.get<Post[]>(`${this.PATH_OF_API}`);
  }

}
