package com.socialmedia.entity;

import javax.persistence.*;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Set;

@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name="user_name")
    private String userName;
    
    @Column(name="user_first_name")
    private String userFirstName;
    
    @Column(name="user_last_name")
    private String userLastName;
    
    @Column(name="user_email")
    private String userEmail;
    
    @Column(name="user_password")
    private String userPassword;
    
    
	@ManyToMany(fetch=FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinTable(
			name="users_roles",
			joinColumns = @JoinColumn(
					name="user_name",referencedColumnName="user_name"),
			inverseJoinColumns=@JoinColumn(name="role_name", referencedColumnName="role_name"))
    private Set<Role> role;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String username) {
        this.userName = username;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }
    
    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Set<Role> getRole() {
        return role;
    }

    public void setRole(Set<Role> role) {
        this.role = role;
    }

}
