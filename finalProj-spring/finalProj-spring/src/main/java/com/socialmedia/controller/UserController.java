package com.socialmedia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.socialmedia.dao.RoleDao;
import com.socialmedia.dao.UserDao;
import com.socialmedia.entity.Role;
import com.socialmedia.entity.User;
import com.socialmedia.repository.UserRepository;
import com.socialmedia.service.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/")
public class UserController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    public void initRoleAndUser() {
        userService.initRoleAndUser();
    }

    @PostMapping({"/registerNewUser"})
    public User registerNewUser(@RequestBody User user) {
        return userService.registerNewUser(user);
    }

    @GetMapping({"/forAdmin"})
    @PreAuthorize("hasRole('Admin')")
    public String forAdmin(){
        return "This URL is only accessible to the admin";
    }

    @GetMapping({"/forUser"})
    @PreAuthorize("hasRole('User')")
    public String forUser(){
        return "This URL is only accessible to the user";
    }
    
    // get all users
 	@GetMapping("/users") 
 	public List<User> getAllUsers(){
 		return userRepository.findAll();
 	}		
 	
 	//for getting one user with unique id
 	@GetMapping("/users/{userName}")
    public ResponseEntity<User> getUserByUserName(@PathVariable("userName") String userName) {
        ResponseEntity<User> matchingUser = new ResponseEntity<User>(userService.getUserByUserName(userName), HttpStatus.OK);
        return matchingUser;
 	}
 	
 	// update user rest api
 	@PutMapping("/users/{userName}")
 	public ResponseEntity<User> updateUser(@PathVariable String userName, @RequestBody User userDetails){
 		
 		User user = userRepository.findByUserName(userName);
 		
 		if(userDetails.getUserName()!=null)
 			user.setUserName(userDetails.getUserName());
 		if(userDetails.getUserFirstName()!=null)
 			user.setUserFirstName(userDetails.getUserFirstName());
 		if(userDetails.getUserLastName()!=null)
 			user.setUserLastName(userDetails.getUserLastName());
 		if(userDetails.getRole()!=null)
 			user.setRole(userDetails.getRole());
 		if(userDetails.getUserEmail()!=null)
 			user.setUserEmail(userDetails.getUserEmail());
 		if(userDetails.getUserPassword()!=null)
 			user.setUserPassword(userDetails.getUserPassword());
 		
 		User updatedUser = userRepository.save(user);
 		return ResponseEntity.ok(updatedUser);
 	}
 	
// 	@PutMapping("/users/updateUser")
// 	@PreAuthorize("hasRole('Admin')")
//	public User updateUser(@RequestBody User user) {
//		return userService.updateUser(user);
//	}
 	
 	// delete user rest api
 	@DeleteMapping("/users/{userName}")
 	@PreAuthorize("hasRole('Admin')")
 	public ResponseEntity<Map<String, Boolean>> deleteUser(@PathVariable String userName){
 		User user = userRepository.findByUserName(userName);
 		userRepository.delete(user);
 		
 		
 		Map<String, Boolean> response = new HashMap<>();
 		response.put("deleted", Boolean.TRUE);
 		return ResponseEntity.ok(response);
 	}
 	
    	
}
