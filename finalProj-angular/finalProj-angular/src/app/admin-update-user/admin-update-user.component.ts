import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-admin-update-user',
  templateUrl: './admin-update-user.component.html',
  styleUrls: ['./admin-update-user.component.css']
})
export class AdminUpdateUserComponent implements OnInit {

  username: string;
  user: User = new User();

  constructor(private userService: UserService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.username = this.route.snapshot.params['username'];

    this.userService.getUserByUserName(this.username).subscribe(data => {
      this.user = data;
    }, error => console.log(error));
  }

  goToUserList(){
    this.router.navigate(['/admin-userlist']);
  }
  
  onSubmit(){
    // this.userService.updateUser(this.username, this.user).subscribe( data =>{
    //   this.goToUserList();
    // }
    // , error => console.log(error));
  }

}
