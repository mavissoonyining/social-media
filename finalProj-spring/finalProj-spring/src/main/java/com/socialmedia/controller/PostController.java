package com.socialmedia.controller;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.socialmedia.entity.ImageModel;
import com.socialmedia.entity.Post;
import com.socialmedia.service.PostService;
import com.socialmedia.repository.PostRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/")
public class PostController {

    @Autowired
    private PostService postService;
    
    @Autowired
    private PostRepository postRepository;
    
    // get all posts
 	@GetMapping("/post") 
 	public List<Post> getAllPost(){
 		return postRepository.findAll();
 	}		

 	//create post
    @PostMapping(value = {"/post"}, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public Post createNewPost(@RequestPart("post") Post post, 
    							@RequestPart("imageFile") MultipartFile[] file) {
        
    	try {
    		Set<ImageModel> images = uploadImage(file);
    		post.setMedia(images);
    		return postService.createNewPost(post);
    	} catch (Exception e) {
    		System.out.println(e.getMessage());
    		return null;
    	}
    }

	public Set<ImageModel> uploadImage(MultipartFile[] multipartFiles) throws IOException {
		
		Set<ImageModel> imageModels = new HashSet<>();
		for(MultipartFile file: multipartFiles) {
			ImageModel imageModel = new ImageModel(
					file.getOriginalFilename(),
					file.getContentType(),
					file.getBytes()
					);
			imageModels.add(imageModel);
			
		}
		
		return imageModels;
		
	}
}
