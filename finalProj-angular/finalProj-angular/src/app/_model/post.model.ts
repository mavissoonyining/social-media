import { FileHandle } from "./file-handle.model";

export interface Post{
	type: string,
	link: string,
	media: FileHandle[],
	caption: string
}