package com.socialmedia.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.socialmedia.entity.Post;

@Repository
public interface PostDao extends CrudRepository<Post, String> {

}
