import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  isRegistered = false;
  isFormValid = false;
  form: FormGroup;

  constructor(
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(){
    
  }

  register(regForm: NgForm) {
    if (!regForm.valid) {
      this.isFormValid = true;
      return;
    }
    this.userService.registerUser(regForm.value).subscribe(
      data =>{
        console.log(data);
        this.isRegistered = true;
        setTimeout(() => {
          this.router.navigate(['/']);
        }, 1000);
        
      },
      error => console.log(error));
  }

}
