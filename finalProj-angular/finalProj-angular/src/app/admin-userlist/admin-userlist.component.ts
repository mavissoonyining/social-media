import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-admin-userlist',
  templateUrl: './admin-userlist.component.html',
  styleUrls: ['./admin-userlist.component.css']
})
export class AdminUserlistComponent implements OnInit {

  users: User[];
  userToUpdate = {
    userName:"",
    userEmail:"",
    userFirstName:"",
    userLastName:"",
    userPassword: ""
  }

  isUpdated = false;

  constructor(private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
    this.getUsers();
  }

  private getUsers(){
    this.userService.getUsersList().subscribe(data => {
      this.users = data;
    });
  }

  edit(user: any){
    this.userToUpdate = user;
    console.log(this.userToUpdate);
  }

  updateUser(){
    this.userService.updateUser(this.userToUpdate.userName, this.userToUpdate).subscribe( data => {
        console.log(data);
        this.isUpdated = true;
      })
  }

  deleteUser(username: string){
    if(confirm("Are you sure to delete?")) {
      this.userService.deleteUser(username).subscribe( data => {
        console.log(data);
        this.getUsers();
      })
    }
    
  }

}
