import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FileHandle } from '../_model/file-handle.model';
import { Post } from '../_model/post.model';
import { PostService } from '../_services/post.service';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  post: Post = {
    type: "",
    link: "",
    media: [],
    caption: ""
  }
  isSubmitted = false;
  isFormValid = false;

  constructor(private postService: PostService,
    private router: Router,
    private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
  }

  addPost(postForm: NgForm){

    const postFormData = this.prepareFormData(this.post);
    this.postService.addPost(postFormData).subscribe(
      (response: Post) =>{
        postForm.reset();
        this.isSubmitted = true;
        console.log(response);
        setTimeout(() => {
          this.router.navigate(['/user-feed']);
        }, 1000);
      },
      (error: HttpErrorResponse) => {
        this.isFormValid = true;
        console.log(error);
      }
    );
  }

  prepareFormData(post: Post): FormData{
    const formData = new FormData();
    formData.append('post', new Blob([JSON.stringify(post)], {type: 'application/json'})
    );

    formData.append('imageFile', new Blob([JSON.stringify(post.media)], {type: 'application/json'}));
    
    console.log(formData)
    return formData;
  }

  onFileSelected(event){
    if(event.target.files){
      const file = event.target.files[0];

      const FileHandle: FileHandle = {
        file: file,
        url: this.sanitizer.bypassSecurityTrustUrl(
          window.URL.createObjectURL(file)
        )
      }
      console.log(this.post);
      // this.post.media.push(FileHandle);
    }
  }

}
