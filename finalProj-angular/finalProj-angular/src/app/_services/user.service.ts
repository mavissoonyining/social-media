import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../user';
import { UserAuthService } from './user-auth.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  PATH_OF_API = 'http://localhost:8080/api';
  baseURL = 'http://localhost:8080/api/users';

  requestHeader = new HttpHeaders({ 'No-Auth': 'True' });
  constructor(
    private httpclient: HttpClient,
    private userAuthService: UserAuthService
  ) {}

  public login(loginData) {
    return this.httpclient.post(this.PATH_OF_API + '/authenticate', loginData, {
      headers: this.requestHeader,
    });
  }

  public registerUser(regData){
    return this.httpclient.post(this.PATH_OF_API + '/registerNewUser', regData, {
      headers: this.requestHeader,
    });  
  }

  public getUsersList(): Observable<User[]>{
    return this.httpclient.get<User[]>(`${this.baseURL}`);
  }

  public getUserByUserName(username: string): Observable<User>{
    return this.httpclient.get<User>(`${this.baseURL}/${username}`);
  }

  public updateUser(username: string, user: User): Observable<Object>{
    return this.httpclient.put(`${this.baseURL}/${username}`, user);
  }

  // public updateUser(student: any) {
  //   return this.httpclient.put(this.baseURL + '/updateStudents', student);
  // }

  public deleteUser(username: string): Observable<Object>{
    return this.httpclient.delete(`${this.baseURL}/${username}`);
  }

  public forUser() {
    return this.httpclient.get(this.PATH_OF_API + '/forUser', {
      responseType: 'text',
    });
  }


  public forAdmin() {
    return this.httpclient.get(this.PATH_OF_API + '/forAdmin', {
      responseType: 'text',
    });
  }

  public roleMatch(allowedRoles): boolean {
    let isMatch = false;
    const userRoles: any = this.userAuthService.getRoles();

    if (userRoles != null && userRoles) {
      for (let i = 0; i < userRoles.length; i++) {
        for (let j = 0; j < allowedRoles.length; j++) {
          if (userRoles[i].roleName === allowedRoles[j]) {
            isMatch = true;
            return isMatch;
          } else {
            return isMatch;
          }
        }
      }
    }
  }
}
